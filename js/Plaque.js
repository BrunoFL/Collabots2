var Plaque = new Phaser.Class({
    Extends: Phaser.GameObjects.Sprite,
    initialize: function Plaque(scene, x, y) {
        this.id = mapMatrix[x][y].id;
        this.key = mapMatrix[x][y].key;
        this.x = centerX + (x - y) * tileWidthHalf;
        this.y = (x + y) * tileHeightHalf;

        Phaser.GameObjects.Sprite.call(this, scene, this.x, this.y, this.key);
        this.depth = this.y;
        plateforms.push(this);
    }
});