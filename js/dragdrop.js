var obj = document.getElementsByClassName('dragable');
for (var i = 0; i < obj.length; i++) {
    touchstart(obj[i]);
    touchmove(obj[i]);
    touchend(obj[i]);
}

var pos = { x: -1, y: -1, obj: null, top: 0, left: 0 };

function getNodeRep(div) {
    var d;
    for (let index = 0; index < div.length; index++) {
        if (div[index].nodeName == 'SPAN') {
            d = div[index];
        }
    }
    return d;
}

function alreadyExist(div) {
    var b = false;
    for (let index = 0; index < div.length; index++) {
        console.log(div[index].nodeName);
        if (div[index].nodeName == 'SPAN') {
            b = true;
        }
    }
    return b;
}

function touchstart(objet) {
    objet.addEventListener('touchstart', function (ev) {
        ev.preventDefault();
        if (ev.targetTouches.length == 1) {
            pos.x = ev.targetTouches[0].clientX;
            pos.y = ev.targetTouches[0].clientY;
            pos.obj = this;
        }
    }, false);
}

function touchmove(objet) {
    objet.addEventListener('touchmove', function (ev) {
        ev.preventDefault();
        if (ev.targetTouches.length == 1) {
            objet.style.zIndex = '110';
            var touch = ev.targetTouches[0];
            pos.obj.style.left = (touch.clientX - pos.x) + 'px';
            pos.obj.style.top = (touch.clientY - pos.y) + 'px';
        }
    }, false);
}

function getReverseImg(path) {
    var reverse = "";
    switch (path) {
        case "assets/icons/forward_red.png":
            reverse = "assets/icons/forward_red_reverse.png";
            break;
        case "assets/icons/p1_red.png":
            reverse = "assets/icons/p1_red_reverse.png";
            break;
        case "assets/icons/p2_red.png":
            reverse = "assets/icons/p2_red_reverse.png";
            break;
        case "assets/icons/rest_red.png":
            reverse = "assets/icons/rest_red_reverse.png";
            break;
        case "assets/icons/rotate_right_red.png":
            reverse = "assets/icons/rotate_right_red_reverse.png";
            break;
        case "assets/icons/rotate_left_red.png":
            reverse = "assets/icons/rotate_left_red_reverse.png";
            break;
        default:
            break;
    }
    return reverse;
}

function touchend(objet) {
    objet.addEventListener('touchend', function (ev) {
        objet.style.zIndex = '0';
        ev.preventDefault();
        pos.obj.style.top = '0px';
        pos.obj.style.left = '0px';
        if (ev.changedTouches.length == 1) {
            var touch = ev.changedTouches[0];
            var target = document.elementFromPoint(touch.clientX, touch.clientY);
            // Check if the zone can be dragable
            if ((target.classList.contains('case') || target.classList.contains('dragable')) && target.classList.length > 1) {
                // Check if same action
                if (target.getAttribute('src') == objet.getAttribute('src')) {
                    // incr the number of repetition
                    var node = getNodeRep(target.parentNode.childNodes);
                    node.textContent = parseInt(node.textContent) + 1;
                    if (objet.classList.contains('2')) {
                        target.parentNode.style.backgroundImage = "url('" + objet.getAttribute('src') + "')";
                        target.parentNode.style.backgroundSize = "100% 100%";
                    } else {
                        target.parentNode.style.backgroundImage = "url('" + getReverseImg(objet.getAttribute('src')) + "')";
                        target.parentNode.style.backgroundSize = "100% 100%";
                    }
                    // Set the number of repetition to 0
                    var node2 = getNodeRep(objet.parentNode.childNodes);
                    var tmp = "0";
                    if (node2 != undefined) {
                        tmp = parseInt(node2.textContent) - 1;
                        node2.textContent = tmp;
                    }
                    // Clone and remove the image
                    if (objet.parentNode.classList.contains('case')) {
                        var w = objet.parentNode;
                        var x = objet.cloneNode(true);
                        w.removeChild(objet);
                        touchstart(x);
                        touchmove(x);
                        touchend(x);
                        target.appendChild(x);
                    }
                }
                else {
                    target.parentNode.style.backgroundImage = "none";
                    // Set the number of repetition to 0
                    var node2 = getNodeRep(objet.parentNode.childNodes);
                    var tmp = "1";
                    if (node2 != undefined) {
                        tmp = node2.textContent;
                        node2.textContent = 0;
                        node2.parentNode.style.backgroundImage = "none";
                    }
                    // Set the number of repetition to 1 or to last case nb of rep
                    var node = getNodeRep(target.childNodes);
                    if (node != undefined && !target.classList.contains('dragable')) {
                        node.textContent = tmp;
                    } else {
                        var node3 = getNodeRep(target.parentNode.childNodes);
                        console.log(node3);
                        if (node3 != undefined) {
                            node3.textContent = tmp;
                            node3.parentNode.style.backgroundImage = "none";
                        }
                    }

                    // Si l'image n'est pas une copie
                    if (!objet.classList.contains('copy')) {
                        if (!target.classList.contains('dragable')) {
                            var x = objet.cloneNode(true);
                            x.classList.add('copy');
                            touchstart(x);
                            touchmove(x);
                            touchend(x);
                            target.appendChild(x);
                            // Si la cible est une image
                        }
                        else if (target.classList.contains('dragable')) {
                            var w = target.parentNode;
                            var x = objet.cloneNode(true);
                            x.classList.add('copy');
                            w.removeChild(target);
                            touchstart(x);
                            touchmove(x);
                            touchend(x);
                            w.appendChild(x);
                        }
                    }
                    else {
                        // Si la cible n'est pas une image
                        if (!target.classList.contains('dragable')) {
                            var w = objet.parentNode;
                            var x = objet.cloneNode(true);
                            w.removeChild(objet);
                            touchstart(x);
                            touchmove(x);
                            touchend(x);
                            target.appendChild(x);
                            // Si la cible est une image
                        }
                        else if (target.classList.contains('dragable')) {
                            var w = target.parentNode;
                            var z = objet.parentNode;
                            var x = objet.cloneNode(true);
                            w.removeChild(target);
                            z.removeChild(objet);
                            touchstart(x);
                            touchmove(x);
                            touchend(x);
                            w.appendChild(x);
                        }
                    }
                }
            }
            else {
                var x = getNodeRep(objet.parentNode.childNodes);
                if (x != undefined) {
                    if (parseInt(x.textContent) > 1) {
                        x.textContent = parseInt(x.textContent) - 1;
                        if (parseInt(x.textContent) == 1) {
                            console.log(objet.parentNode);
                            objet.parentNode.style.backgroundImage = "none";
                        }
                    } else {
                        if (objet.classList.contains('copy')) {
                            objet.parentNode.removeChild(objet);
                        }
                        x.textContent = parseInt(x.textContent) - 1;
                    }
                }
            }
        }
    }, false);
}

function goLevel() {
    document.getElementById('menu').style.display = 'none';
    document.getElementById('chLevels').style.display = 'block';
    //loadZones();
}

function goMenu() {
    document.getElementById('menu').style.display = 'block';
    document.getElementById('chLevels').style.display = 'none';
    //loadZones();
}
