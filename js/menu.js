var cptMessage = 0;
var currentLevelIndex;

document.getElementById('bcMessage').addEventListener('click', function () {
    nextMessage(currentLevelIndex);
});

function loadLevel(i) {

    currentLevelIndex = i;
    cptMessage = 0;
    document.getElementById('chLevels').style.display = 'none';
    /*if (config.level[i].message && config.level[i].message.length > 0) {
        document.getElementById('bcM')
    }*/
    chargerNiveau(currentLevelIndex + 1);
    loadMessages(currentLevelIndex);
    loadZones(currentLevelIndex);
}

function loadMessages(index) {

    /*var el = document.getElementById('bcMessage');
    elClone = el.cloneNode(true);

    el.parentNode.replaceChild(elClone, el);*/
    //    document.getElementById('bcMessage').removeEventListener('click', nextMessage);
    cptMessage = 0;
    document.getElementById('bcMessage').style.display = 'block';
    nextMessage(index);
}

function nextMessage(i) {
    var messageDiv = document.getElementById('message');
    var messageDiv2 = document.getElementById('message2');
    if (config.level[i].message[cptMessage]) {
        messageDiv.innerHTML = config.level[i].message[cptMessage];
        messageDiv2.innerHTML = config.level[i].message[cptMessage];
        cptMessage++;

    }
    else {

        document.getElementById("bcMessage").style.display = 'none';

    }
}


var menu = (function () {



    /**
     *  [Private]
     *  Displays the page whose id is given in parameter. Hides all the others. 
     */
    var showPage = function (bcID) {
        // all front pages share the same class name ("bcPage")
        var bcPages = document.getElementsByClassName("bcPage");
        for (var i = 0; i < bcPages.length; i++) {
            bcPages[i].style.display = (bcPages[i].id == bcID) ? "block" : "none";
        }
    }


    /** 
     *  Shows the level selection screen
     */
    this.showLevels = function () {
        isPlaying = false;
        this.generateLevelMenu(1);
        showPage("chLevels");
    }


    /*************************************************************************
     *                          UTILITARY FUNCTIONS
     *************************************************************************/

    /** Computes the level page content */
    this.generateLevelMenu = function (g) {

        var groupe = g;
        var subtitles = [1, 2, 3, 4];
        var cesure = [3, 3, 3, 3];
        // titre
        var html = "<img src='./assets/img/niv.png' id='choix'>"; //Choix du niveau</h2>";

        //html += "<h3>" + subtitles[groupe-1] + "</h3>";

        // flèches gauches/droites + contenu
        if (groupe > 1) {
            html += '<img id="flechePrecedent" src="./assets/img/fleche_left.png" class="fleche" onclick="menu.generateLevelMenu(' + (groupe - 1) + ')">';
        }
        if (groupe < subtitles.length) {
            html += '<img id="flecheSuivant" src="./assets/img/fleche_right.png" class="fleche" onclick="menu.generateLevelMenu(' + (groupe + 1) + ')">';
        }

        html += '<div id="levelContent">';

        var nb = 0;
        // liste des niveaux
        for (var i = 0; i < config.level.length; i++) {
            if (config.level[i].groupe == groupe) {
                nb++;
                var note = localStorage.getItem("note_level" + (i));
                html += "<div class='iconeNiveau";
                if (!note) {
                    html += " grise";
                    note = 0;
                }
                html += "' onclick='loadLevel(" + (i) + ")'>"
                    + "<img src='./assets/img/levels_icons/" + (i + 1) + ".png' class='numero'><br>"
                    + "<img src='./assets/img/etoile" + (note < 1 ? "Off" : "") + ".png' class='etoile'>"
                    + "<img src='./assets/img/etoile" + (note < 2 ? "Off" : "") + ".png' class='etoile'>"
                    + "<img src='./assets/img/etoile" + (note < 3 ? "Off" : "") + ".png' class='etoile'>"
                    + "</div>";
                if (nb == cesure[groupe - 1]) {
                    html += "<br>";
                }
            }
        }

        html += '</div>' +
            '<div id="retour" onclick="goMenu()">Retour</div>';
        document.getElementById("chLevels").innerHTML = html;
    }

    // returns the current object
    return this;

})();

function backToMenu() {
    document.getElementById('menu').style.display = 'block';
}




