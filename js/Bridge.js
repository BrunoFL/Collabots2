var Bridge = new Phaser.Class({
    Extends: Phaser.GameObjects.Sprite,
    initialize: function Bridge(scene, x, y) {
        this.id = mapMatrix[x][y].id;
        this.key = mapMatrix[x][y].key;
        this.nx = x;
        this.ny = y;
        this.x = centerX + (x - y) * tileWidthHalf;
        this.y = (x + y) * tileHeightHalf;
        this.on = 0;

        bridges[this.id] = this;


        Phaser.GameObjects.Sprite.call(this, scene, this.x, this.y, this.key);
        this.depth = this.y;
        plateforms.push(this);

        this.anims.load(this.key + "0");
        this.anims.load(this.key + "1");
        this.anims.play(this.key + "0");
    },

    activation: function (plok) {
        if (this.on != plok) {
            this.on = plok;
            if (this.on == 0) {
                this.anims.play(this.key + "0");
            } else {
                this.anims.play(this.key + "1");
            }
        }
    }
});