// Class Robot gere l'affichage
var Robot = new Phaser.Class({
    Extends: Phaser.GameObjects.Sprite, // Hérite de sprite
    initialize: function Robot(scene, level, estPetit) {
        this.alive = true;
        this.scene = scene;
        this.id = estPetit ? 2 : 1;
        this.nx = this.oriX = level.player[this.id - 1].x;
        this.ny = this.oriY = level.player[this.id - 1].y;
        this.direction = this.oriDirection = directions[level.player[this.id - 1].direction];
        this.offsetY = 0;
        this.estPetit = estPetit;
        this.autre = null;
        this.surAutre = 0;
        this.monteSurCaseHaute = false;

        this.update();

        Phaser.GameObjects.Sprite.call(this, scene, this.x, this.y, 'man' + this.id);
        this.setScale(0.75);

        this.flares = scene.add.particles('flares');
        this.emitter = this.flares.createEmitter({
            frame: 'blue',
            angle: { min: 120, max: 60 },
            speed: 400,
            alphaEnd: 0,
            scale: { start: 0.3, end: 0.7 },
            lifespan: { min: 80, max: 200 },
            on: false,
            blendMode: 'ADD'
        });
        this.emitter.startFollow(this);
        this.emitter.followOffset.y = this.height * this.scaleX * 0.4;

        this.teleport = this.flares.createEmitter({
            frame: 'red',
            angle: 270,
            speed: 400,
            scale: { start: 0.3, end: 0.7 },
            lifespan: { min: 80, max: 250 },
            on: false,
            blendMode: 'ADD'
        });
        this.teleport.startFollow(this);
        this.teleport.followOffset.y = this.height * this.scaleX * 0.4;

        this.explosion1 = this.flares.createEmitter({ frame: 'blue', speed: 200, scale: 0.6, blendMode: 'ADD', lifespan: 400, on: false });
        this.explosion1.startFollow(this);
        this.explosion1.followOffset.y = this.height * this.scaleX * 0.4;

        this.explosion2 = this.flares.createEmitter({ frame: 'red', speed: 180, scale: 0.8, blendMode: 'ADD', lifespan: 900, on: false });
        this.explosion2.startFollow(this);

        this.explosion3 =
            this.flares.createEmitter({ frame: 'yellow', speed: 200, scale: { min: 0, max: 1 }, blendMode: 'ADD', lifespan: 1000, on: false });
        this.explosion3.startFollow(this);

        this.anims.load('idleSE' + this.id);
        this.anims.load('walkSE' + this.id);
        this.anims.load('idleSW' + this.id);
        this.anims.load('walkSW' + this.id);
        this.anims.load('idleNE' + this.id);
        this.anims.load('walkNE' + this.id);
        this.anims.load('idleNW' + this.id);
        this.anims.load('walkNW' + this.id);

        this.anims.play('idle' + this.direction.key + this.id);

        this.depth = this.y + 90;
    },

    setAutre: function (autre) {
        this.autre = autre;
    },

    // Change la direction et lance l'animation
    rotateL: function () {
        this.direction = directions[this.direction.rotateL];
        this.anims.play('idle' + this.direction.key + this.id);
    },

    // Change la direction et lance l'animation
    rotateR: function () {
        this.direction = directions[this.direction.rotateR];
        this.anims.play('idle' + this.direction.key + this.id);
    },

    // Avance d'une case en fonction de la direction
    walk: function () {
        var newx = Math.round(this.nx), newy = Math.round(this.ny);

        switch (this.direction.key) {
            case 'SW': newy++; break;
            case 'SE': newx++; break;
            case 'NW': newx--; break;
            case 'NE': newy--; break;
        }

        // Verification
        if (newx >= mapMatrix.length || newy >= mapMatrix[0].length || newx < 0 || newy < 0)
            return;


        var dest = mapMatrix[newx][newy];

        // Si petit
        if (this.estPetit) {
            // Si une grande tuile ou si joueur deja present
            if (config.upTiles.includes(dest) || (newx == this.autre.nx && newy == this.autre.ny))
                return;
        }
        else {
            // Monte sur case haute
            if (config.upTiles.includes(dest) && !config.upTiles.includes(mapMatrix[this.nx][this.ny])) {
                setTimeout(function () {
                    for (var i = 0; i < 5; i++)
                        if (player1.estPetit) {
                            player2.emitter.emitParticle();
                            player2.flares.depth = player2.depth + 2;
                        }
                        else {
                            player1.particle.depth = player1.depth + 2;
                            player1.flares.emitParticle();
                        }
                }, 300);
            }

            // Si deja sur autre "descend"
            if (this.surAutre > 0) {
                this.scene.tweens.add({ targets: this, offsetY: 0, ease: 'Quad.easeOut', duration: 600, repeat: 0, surAutre: 0 });
            }
            else {
                // Si petit deja sur case monte
                if (newx == this.autre.nx && newy == this.autre.ny) {
                    var off = this.autre.height * this.autre.scaleX * 0.6;
                    this.scene.tweens.add({ targets: this, offsetY: -off, surAutre: 1, ease: 'Quad.easeOut', duration: 600, repeat: 0 });
                }
            }
        }

        // Jouer animations
        this.anims.play('walk' + this.direction.key + this.id);

        // Permet de gerer le deplacement d'une facon jolie
        // En mettant a jour peu à peu nx
        this.scene.tweens.add({ targets: this, nx: newx, ny: newy, ease: 'Quad.easeOut', duration: 800, repeat: 0 });
    },

    // Met à jour la coordonnée X
    updateCoordX: function () {
        this.x = centerX + (this.nx - this.ny) * tileWidthHalf;
    },

    // Met à jour la coordonnée Y
    updateCoordY: function () {
        this.y = centerY + (this.nx + this.ny) * tileHeightHalf - (tileHeight * 1.22);
        this.depth = this.y + 64 + Math.round(this.surAutre) * 2;

        // Fix for elevated floor
        var tuile = mapMatrix[Math.round(this.nx)][Math.round(this.ny)];
        if (config.upTiles.includes(tuile) || (typeof (tuile) == 'object' && config.upTiles.includes(tuile.key))) {
            this.y -= tileHeight * 0.7;
        }

        if (!this.estPetit) {
            this.y += this.offsetY;
        }
    },


    // Met à jour les coordonnées
    update: function () {
        this.updateCoordX();
        this.updateCoordY();
        var tuile = mapMatrix[Math.round(this.nx)][Math.round(this.ny)];
        if (this.flares)
            this.flares.depth = this.depth + 2;
        if (this.surAutre > 0) {
            this.flares.depth = this.depth + 2;
            this.monteSurCaseHaute = false;
            this.emitter.emitParticle();
        }

        // Explosion
        var obj = mapMatrix[Math.round(this.nx)][Math.round(this.ny)];
        if (!this.estPetit && !this.surAutre && this.alive) {
            if ('water' == obj) {
                this.explosion();
            }


            if (obj == '[object Object]' && obj.key.includes('bridge') && bridges[obj.id].on == 0) {
                this.explosion();
            }
        }
    },

    // Remet comme debut les coordonnées
    reset: function () {
        this.direction = directions[levelinfo.player[this.id - 1].direction];
        this.anims.play('idle' + this.direction.key + this.id);
        this.nx = this.oriX = levelinfo.player[this.id - 1].x;
        this.ny = this.oriY = levelinfo.player[this.id - 1].y;
        this.surAutre = 0;
        this.offsetY = 0;
        this.alive = true;
        this.visible = true;
    },

    explosion: function () {
        for (var i = 0; i < 25; i++) {
            this.explosion1.emitParticle();
            this.explosion2.emitParticle();
            this.explosion3.emitParticle();
        }
        this.alive = false;
        this.visible = false;
    },

    teleportTo: function (x, y) {
        this.nx = x;
        this.ny = y;
        this.teleport.emitParticle();
        this.teleport.emitParticle();
        this.teleport.emitParticle();
    }
});