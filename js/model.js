var instructions1 = [];
var instructions2 = [];
var cptInstr = 0;
var cptInstr1 = 0;
var cptInstr2 = 0;
var execFinie = false;


function win() {
	console.log("terminé !!");

	if ((config.level[currentLevelIndex].map[player1.nx][player1.ny] == "endRed" ||
		config.level[currentLevelIndex].map[player1.nx][player1.ny] == "endRedHigh") && (config.level[currentLevelIndex].map[player2.nx][player2.ny] == "endBlue" || config.level[currentLevelIndex].map[player2.nx][player2.ny] == "endBlueHigh")) {
		console.log("victoire ! ");
		document.getElementById("bcMessage").style.display = 'block';
		document.getElementById("message").innerHTML = "Bien joué ! <button type='button' id='suivant' class='btnSuivant'>Niveau suivant</button>";
		document.getElementById("message2").innerHTML = "Bien joué ! ";
		document.getElementById('suivant').addEventListener("click", function () {
			cptMessage = 0;
			loadLevel(currentLevelIndex + 1, 0);
		});
	}
}


function execute() {
	/*player1.alive = true;
	player2.alive = true;*/
	// on execute une instructions à tour de role, en commençant par le joueur 1
	var max = Math.max(instructions1.length, instructions2.length);
	var totalInstrs = instructions1.length + instructions2.length;
	var i = 0;
	var joueur = 1;
	deplacements(1);
};


function fillWithProc(joueur, procNumber) {
	var proc;
	if (procNumber == 1) {
		proc = document.getElementById('procJ' + joueur);
	}
	else {
		proc = document.getElementById('proc2J' + joueur);
	}

	var instrs = proc.children;
	console.log(instrs);
	var arr = Array.from(instrs);
	arr.forEach(function (instr) {
		var nbrInstr = instr.firstElementChild.innerHTML;
		if (nbrInstr != 0) {
			var instrAEffectue = instr.firstElementChild.nextSibling;
			while (instrAEffectue && instrAEffectue.nodeType != 1) {
				instrAEffectue = instrAEffectue.nextSibling
			}

			var instrTxt = instrAEffectue.src;
			var toPush;
			// analyse de l'instr à effectuer
			if (instrTxt.includes('forward')) {
				toPush = 'forward';
			}
			else if (instrTxt.includes('rotate_right')) {
				toPush = 'rotateR';
			}
			else if (instrTxt.includes('rotate_left')) {
				toPush = 'rotateL';
			}
			else if (instrTxt.includes('rest')) {
				toPush = 'rest';
			}
			if (joueur == 1) {
				for (var i = 0; i < nbrInstr; i++) {
					instructions1.push(toPush);
				}
			}
			else {
				for (var i = 0; i < nbrInstr; i++) {
					instructions2.push(toPush);
				}
			}
		}
	});
};



function deplacements(joueur) {
	var tabInstrs;
	var player;
	var index;
	var color;

	if (!player1.alive || !player2.alive) {
		instructions1 = [];
		instructions2 = [];
		return;
	};

	if (joueur == 1) {
		tabInstrs = instructions1;
		player = player1;
		index = cptInstr1;
		color = 'red';
	}
	else if (joueur == 2) {
		tabInstrs = instructions2;
		player = player2;
		index = cptInstr2;
		color = 'blue';
	};

	document.getElementById('currentInstrJ' + joueur + '').src = 'assets/icons/blank_' + color + '.png';
	switch (tabInstrs[index]) {
		case 'forward':
			document.getElementById('currentInstrJ' + joueur + '').src = 'assets/icons/forward_' + color + '.png';
			player.walk();
			break;
		case 'rotateR':
			document.getElementById('currentInstrJ' + joueur + '').src = 'assets/icons/rotate_right_' + color + '.png';
			player.rotateR();
			break;
		case 'rotateL':
			document.getElementById('currentInstrJ' + joueur + '').src = 'assets/icons/rotate_left_' + color + '.png';
			player.rotateL();
			break;
		case 'rest': document.getElementById('currentInstrJ' + joueur + '').src = 'assets/icons/rest_' + color + '.png'; break;
	};

	if (joueur == 1) {
		cptInstr1++;
		if (cptInstr2 < instructions2.length) {
			setTimeout(function () {
				deplacements(2);
			}, 1000);
		}
		else if (cptInstr1 < instructions1.length) {
			setTimeout(function () {
				deplacements(1);
			}, 1000);
		}
		else {
			setTimeout(function () {
				win();
			}, 1000);
		}
		setTimeout(function () {
			change1();
		}, 950);
	}
	else {
		cptInstr2++;
		if (cptInstr1 < instructions1.length) {
			setTimeout(function () {
				deplacements(1);
			}, 1000);
		}
		else if (cptInstr2 < instructions2.length) {
			setTimeout(function () {
				deplacements(2);
			}, 1000);
		}
		else {
			setTimeout(function () {
				win();
			}, 1000);
		}
		setTimeout(function () {
			change2();
		}, 950);
	}
}
/*else {
	//detection de victoire

}*/




function getInstructions() {
	reset();
	cptInstr1 = 0;
	cptInstr2 = 0;
	instructions1 = [];
	instructions2 = [];
	document.getElementById('currentInstrJ1').src = 'assets/icons/blank_red.png';
	document.getElementById('currentInstrJ2').src = 'assets/icons/blank_blue.png';


	var joueur = 1;
	while (joueur < 3) {
		var zoneATraiter = document.getElementById('progJ' + joueur);
		var main = zoneATraiter.firstElementChild;
		var instrs = main.children;
		[].forEach.call(instrs, function (instr) {			  // instr : div class = case
			var nbrInstr = instr.firstElementChild.innerHTML; // donne le nombre d'instrs a effectuer
			if (nbrInstr != 0) {
				var instrAEffectue = instr.firstElementChild.nextSibling;
				while (instrAEffectue && instrAEffectue.nodeType != 1) {
					instrAEffectue = instrAEffectue.nextSibling
				}
				var instrTxt = instrAEffectue.src;
				var toPush;
				var toAdd = true;
				// analyse de l'instr à effectuer
				if (instrTxt.includes('forward')) {
					toPush = 'forward';
				}
				else if (instrTxt.includes('rotate_right')) {
					toPush = 'rotateR';
				}
				else if (instrTxt.includes('rotate_left')) {
					toPush = 'rotateL';
				}
				else if (instrTxt.includes('rest')) {
					toPush = 'rest';
				}
				else if (instrTxt.includes('p1')) {
					fillWithProc(joueur, 1);
					toAdd = false;
				}
				else if (instrTxt.includes('p2')) {
					fillWithProc(joueur, 2);
					toAdd = false;
				}
				if (toAdd == true) {
					if (joueur == 1) {
						for (var i = 0; i < nbrInstr; i++) {
							instructions1.push(toPush);
						}
					}
					else {
						for (var i = 0; i < nbrInstr; i++) {
							instructions2.push(toPush);
						}
					}
				}
			}
		});
		joueur++;
	}

	// execute();
	setTimeout(function () {
		execute();
	}, 1000);
};

function change1() {
	document.getElementById('currentInstrJ1').src = 'assets/icons/blank_red.png';
};

function change2() {
	document.getElementById('currentInstrJ2').src = 'assets/icons/blank_blue.png';
};


function executeJ2() {
	console.log(instructions2);

	setTimeout(deplacements, 1000);
};

var launchButton = document.getElementById('launchButton');
launchButton.addEventListener('touchstart', getInstructions, false);
