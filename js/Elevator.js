var Elevator = new Phaser.Class({
    Extends: Phaser.GameObjects.Sprite,
    initialize: function Elevator(scene, x, y) {
        this.id = mapMatrix[x][y].id;
        this.key = mapMatrix[x][y].key;
        this.nx = x;
        this.ny = y;
        this.x = centerX + (x - y) * tileWidthHalf;
        this.y = (x + y) * tileHeightHalf;
        this.on = 0;

        elevators[this.id] = this;

        Phaser.GameObjects.Sprite.call(this, scene, this.x, this.y, this.key);
        this.depth = this.y;
        plateforms.push(this);
    }
});