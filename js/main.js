var widthCVS = window.innerWidth;
var heigthCVS = window.innerHeight * 0.6;
var scene;
var hammertime;
var config = {
    type: Phaser.AUTO,
    backgroundColor: '#0d0d0d',
    parent: 'cvs',
    scene: { preload: preload, create: create, update: update },
};

var plateforms = [], tile0;
var config, level = 1, buildMapOk = false;
var player1, player2, players = [];
var controls, particles;
var levelinfo, mapMatrix, centerX, centerY = 16;
var tileWidth = 100, tileHeight = 50;
var tileWidthHalf = tileWidth / 2, tileHeightHalf = tileHeight / 2;
var cptDestroy = 0, destroyFinit = false;
var bridges = [], elevators = [];

var directions = {
    SW: { key: 'SW', rotateL: 'SE', rotateR: 'NW' },
    SE: { key: 'SE', rotateL: 'NE', rotateR: 'SW' },
    NE: { key: 'NE', rotateL: 'NW', rotateR: 'SE' },
    NW: { key: 'NW', rotateL: 'SW', rotateR: 'NE' }
};


var game = new Phaser.Game(config);

function preload() {
    this.load.json('config', 'config/config.json');
    this.load.path = 'assets/';
    this.load.spritesheet('man', 'character/player.png', { frameWidth: 64, frameHeight: 128 });
    this.load.spritesheet('man1', 'character/sprite_blue.png', { frameWidth: 100, frameHeight: 217 });
    this.load.spritesheet('man2', 'character/sprite_red.png', { frameWidth: 100, frameHeight: 217 });

    this.load.image('dirtHigh', 'tiles/dirtHigh.png');
    this.load.image('dirt', 'tiles/dirt.png');
    this.load.image('grass', 'tiles/grass.png');
    this.load.image('grassHigh', 'tiles/grassHigh.png');
    this.load.image('water', 'tiles/water.png');
    this.load.image('endBlue', 'tiles/endPointBlue.png');
    this.load.image('endRed', 'tiles/endPointRed.png');
    this.load.image('endRedHigh', 'tiles/endPointRedHigh.png');
    this.load.image('endBlueHigh', 'tiles/endPointBlueHigh.png');
    this.load.image('water', 'tiles/water.png');

    this.load.image('bridgeEast', 'tiles/bridgeEast.png');
    this.load.image('bridgeNorth', 'tiles/bridgeNorth.png');
    this.load.image('bridgeNorthYellow', 'tiles/bridgeNorthYellow.png');
    this.load.image('bridgeEastYellow', 'tiles/bridgeEastYellow.png');

    this.load.image('elevator', 'tiles/elevator.png');
    this.load.image('elevatorHigh', 'tiles/elevatorHigh.png');
    this.load.image('elevatorPurple', 'tiles/elevatorPurple.png');
    this.load.image('elevatorHighPurple', 'tiles/elevatorHighPurple.png');
    this.load.image('elevatorYellow', 'tiles/elevatorYellow.png');
    this.load.image('elevatorHighYellow', 'tiles/elevatorHighYellow.png');

    this.load.image('plaque', 'tiles/plaque.png');
    this.load.image('plaqueYellow', 'tiles/plaqueYellow.png');
    this.load.image('plaquePurple', 'tiles/plaquePurple.png');

    this.load.image('spark', 'icons/blue.png');
    this.load.atlas('flares', 'icons/flares.png', 'icons/flares.json');

    this.load.path = '';
}

function create() {
    scene = this;
    config = this.cache.json.get('config');

    levelinfo = config.level[level - 1];
    mapMatrix = levelinfo.map;

    // Deplacement map au toucher
    hammertime = new Hammer(document.getElementById('cvs'));
    hammertime.get('rotate').set({ enable: false });
    hammertime.get('swipe').set({ enable: false });
    hammertime.get('press').set({ enable: false });
    hammertime.get('doubletap').set({ enable: true });
    hammertime.get('pinch').set({ enable: true });
    hammertime.get('pan').set({ direction: Hammer.DIRECTION_ALL });
    hammertime.on('panleft panright panup pandown pinchin pinchout doubletap', function (ev) {
        if (ev.type == 'panright' || ev.type == 'panleft') {
            scene.cameras.main.scrollX += -ev.srcEvent.movementX;
        } else if (ev.type == 'panup' || ev.type == 'pandown') {
            scene.cameras.main.scrollY += -ev.srcEvent.movementY;
        } else if (ev.type == 'pinchin') {
            if (scene.cameras.main.zoom > 0.35) {
                scene.cameras.main.zoom -= 0.05;
            }
            else {
                scene.cameras.main.zoom = 0.35;
            }
        } else if (ev.type == 'pinchout') {
            if (scene.cameras.main.zoom < 4) {
                scene.cameras.main.zoom += 0.05;
            }
            else {
                scene.cameras.main.zoom = 4;
            }
        } else if (ev.type == 'doubletap') {
            resetCamera();
        }
    });


    // Crée les animations
    var idleSW1 = { key: 'idleSW1', frames: scene.anims.generateFrameNumbers('man1', { frames: [0] }) };
    var walkSW1 = { key: 'walkSW1', frames: scene.anims.generateFrameNumbers('man1', { frames: [0, 1, 2, 3, 4, 5, 6] }), frameRate: 15 };
    var idleSE1 = { key: 'idleSE1', frames: scene.anims.generateFrameNumbers('man1', { frames: [7] }) };
    var walkSE1 = { key: 'walkSE1', frames: scene.anims.generateFrameNumbers('man1', { frames: [7, 8, 9, 10, 11, 12, 13] }), frameRate: 15 };
    var idleNW1 = { key: 'idleNW1', frames: scene.anims.generateFrameNumbers('man1', { frames: [14] }) };
    var walkNW1 = { key: 'walkNW1', frames: scene.anims.generateFrameNumbers('man1', { frames: [14, 15, 16, 17, 18, 19, 20] }), frameRate: 15 };
    var idleNE1 = { key: 'idleNE1', frames: scene.anims.generateFrameNumbers('man1', { frames: [21] }) };
    var walkNE1 = { key: 'walkNE1', frames: scene.anims.generateFrameNumbers('man1', { frames: [21, 22, 23, 24, 25, 26, 27] }), frameRate: 15 };

    this.anims.create(idleSW1);
    this.anims.create(walkSW1);
    this.anims.create(idleSE1);
    this.anims.create(walkSE1);
    this.anims.create(idleNW1);
    this.anims.create(walkNW1);
    this.anims.create(idleNE1);
    this.anims.create(walkNE1);

    var idleSW2 = { key: 'idleSW2', frames: scene.anims.generateFrameNumbers('man2', { frames: [0] }) };
    var walkSW2 = { key: 'walkSW2', frames: scene.anims.generateFrameNumbers('man2', { frames: [0, 1, 2, 3, 4, 5, 6] }), frameRate: 15 };
    var idleSE2 = { key: 'idleSE2', frames: scene.anims.generateFrameNumbers('man2', { frames: [7] }) };
    var walkSE2 = { key: 'walkSE2', frames: scene.anims.generateFrameNumbers('man2', { frames: [7, 8, 9, 10, 11, 12, 13] }), frameRate: 15 };
    var idleNE2 = { key: 'idleNE2', frames: scene.anims.generateFrameNumbers('man2', { frames: [14] }) };
    var walkNE2 = { key: 'walkNE2', frames: scene.anims.generateFrameNumbers('man2', { frames: [14, 15, 16, 17, 18, 19, 20] }), frameRate: 15 };
    var idleNW2 = { key: 'idleNW2', frames: scene.anims.generateFrameNumbers('man2', { frames: [21] }) };
    var walkNW2 = { key: 'walkNW2', frames: scene.anims.generateFrameNumbers('man2', { frames: [21, 22, 23, 24, 25, 26, 27] }), frameRate: 15 };
    this.anims.create(idleSW2);
    this.anims.create(walkSW2);
    this.anims.create(idleSE2);
    this.anims.create(walkSE2);
    this.anims.create(idleNW2);
    this.anims.create(walkNW2);
    this.anims.create(idleNE2);
    this.anims.create(walkNE2);

    // Animation bridge
    for(var i = 0; i < 3; i++){
        for (var j = 0; j < 2; j++){
            var color, direct;
            switch (i) {
                case 1:
                    color = "Yellow";
                    break;
                case 2:
                    color = "Purple";
                    break;
            
                default:
                    color = "";
                    break;
            }

            switch (j) {
                case 1:
                    direct = "East";
                    break;
            
                default:
                    direct = "North";
                    break;
            }
        }
        var animBridgeOff = {key:'bridge'+direct+color+'1',
        frames: [{key: 'bridge'+direct+color}]};

        var animBridgeOn = {
            key:'bridge'+direct+color+'0',
            frames: [{key: 'bridge'+direct+color}, {key:'water'}],
            frameRate: 2,
            repeat: -1};

        this.anims.create(animBridgeOff);
        this.anims.create(animBridgeOn);

    }

    particles = this.add.particles('spark');
    buildCamera();

    player1 = scene.add.existing(new Robot(scene, levelinfo, true));
    player2 = scene.add.existing(new Robot(scene, levelinfo, false));
    player1.setAutre(player2);
    player2.setAutre(player1);
    players.push(player1, player2);
    chargerNiveau(1);
}


// Met a jour les joueurs
function update(time, delta) {
    if (buildMapOk){
        updatePlaques();
        players.forEach(function (player) {
            player.update();
            updatePlaques();
        });
    }
    controls.update(delta);
}

// Affiche la carte
function buildMap() {
    buildMapOk = false;
    var mapWidth = mapMatrix.length;
    var mapHeight = mapMatrix[0].length;

    centerX = mapWidth * tileWidthHalf;

    for (var y = 0; y < mapHeight; y++) {
        for (var x = 0; x < mapWidth; x++) {
            // Build map
            mapMatrix = levelinfo.map;
            var mapWidth = mapMatrix.length;
            var mapHeight = mapMatrix[0].length;

            centerX = mapWidth * tileWidthHalf;

            if (mapMatrix[x][y] == '')
                continue;

            if (typeof (mapMatrix[x][y]) == "object") {
                if (mapMatrix[x][y].key.includes('bridge')) {
                    scene.add.existing(new Bridge(scene, x, y));
                } else if (mapMatrix[x][y].key.includes('plaque')) {
                    scene.add.existing(new Plaque(scene, x, y));
                } else {
                    scene.add.existing(new Elevator(scene, x, y));
                }
                continue;
            }
            var tx = (x - y) * tileWidthHalf;
            var ty = (x + y) * tileHeightHalf;

            var tile = scene.add.sprite(centerX + tx, ty, mapMatrix[x][y]);
            plateforms.push(tile);

            tile.depth = ty;

            if (x == 0 && y == 0) {
                // tile.alpha = 0.2;
                tile0 = tile;
            }
        }
    }
    buildMapOk = true;
}

// Crée la camera
function buildCamera() {
    // Build camera
    var cursors = scene.input.keyboard.createCursorKeys();

    var controlConfig = {
        camera: scene.cameras.main,
        left: cursors.left,
        right: cursors.right,
        up: cursors.up,
        down: cursors.down,
        zoomIn: scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Q),
        zoomOut: scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.E),
        acceleration: 0.04,
        drag: 0.0005,
        maxSpeed: 0.7
    };

    scene.input.keyboard.on('keydown_Z', function (event) {
        scene.cameras.main.zoom -= 0.05;
    }, scene);
    scene.input.keyboard.on('keydown_X', function (event) {
        scene.cameras.main.zoom += 0.05;
    }, scene);

    controls = new Phaser.Cameras.Controls.Smoothed(controlConfig);
}


/// Fonctions temporaires pour tester les joueurs
function walk() {
    players.forEach(function (p) {
        p.walk();
    });
}
function rotateR() {
    players.forEach(function (p) {
        p.rotateR();
    });
}
function rotateL() {
    players.forEach(function (p) {
        p.rotateL();
    });
}
///

function reset() {
    scene.cameras.main.flash(400);
    scene.cameras.main.shake(400);
    players.forEach(function (p) {
        p.reset();
    });
}

function resetCamera() {
    scene.cameras.main.scrollX = -widthCVS / 2 - 20;
    scene.cameras.main.scrollY = -heigthCVS / 2;
    scene.cameras.main.zoom = levelinfo.cameraZoom;
}

function chargerNiveau(niveau) {
    buildMapOk = false;
    document.getElementById('chLevels').style.display = 'none';
    level = niveau;
    levelinfo = config.level[level - 1];
    bridges = [];

    //Suppression ancien level
    var cpt = scene.children.list.length;
    setTimeout(destroyList, 15);
    cptDestroy = 0;
}

function destroyList() {
    var obj = scene.children.list[cptDestroy];
    if (obj != undefined) {
        if (obj.type == "Sprite" && !(obj instanceof Robot)) {
            obj.destroy();
        } else {
            cptDestroy++;
        }
        obj = scene.children.list[cptDestroy];
        if (obj != undefined) {
            setTimeout(destroyList, 15);
        } else {
            destroyFinit = true;
            setTimeout(function () {
                resetCamera();
                buildMap();

                player1.reset();
                player2.reset();
            }, 50);
        }
    }
}

function updatePlaques() {
    bridges.forEach(bridge => {
        bridge.activation(0);
    });

    var mapWidth = mapMatrix.length;
    var mapHeight = mapMatrix[0].length;
    var obj = mapMatrix[Math.round(player1.nx)][Math.round(player1.ny)];
    if (typeof (obj) == 'object' && obj.key.includes('plaque')) {
        if (obj.type == 'bridge' && obj.id < bridges.length) {
            bridges[obj.id].activation(1);
        } else if (obj.type == "elevator") {
            // Si sur elevator in
            var obj2 = mapMatrix[Math.round(player2.nx)][Math.round(player2.ny)];
            if (typeof (obj2) == "object" && obj.id == obj2.id && obj2.key.includes('elevator') && obj2.way == 'in') {
                //Teleportation
                for (var y = 0; y < mapHeight; y++) {
                    for (var x = 0; x < mapWidth; x++) {
                        var obj3 = mapMatrix[x][y];
                        if (typeof (obj3) == "object" && obj3.id == obj.id && (obj3.key.includes("elevator") || obj3.key.includes("Elevator")) && obj3.way == "out") {
                            player2.teleportTo(x, y);
                        }
                    }
                }
            }
        }
    }

    obj = mapMatrix[Math.round(player2.nx)][Math.round(player2.ny)];
    if (typeof (obj) == "object" && obj.key.includes('plaque')) {
        if (obj.type == "bridge" && obj.id < bridges.length) {
            bridges[obj.id].activation(1);
        } else if (obj.type == "elevator") {
            // Si sur elevator in
            var obj2 = mapMatrix[Math.round(player1.nx)][Math.round(player1.ny)];
            if (typeof (obj2) == 'object' && obj.id == obj2.id && obj2.key.includes('elevator') && obj2.way == 'in') {
                //Teleportation

                for (var y = 0; y < mapHeight; y++) {
                    for (var x = 0; x < mapWidth; x++) {
                        var obj3 = mapMatrix[x][y];
                        if (typeof (obj3) == 'object' && obj3.id == obj.id && obj3.key.includes('elevator') && obj3.way == 'out') {
                            player1.teleportTo(x, y);
                        }
                    }
                }
            }
        }
    }
}