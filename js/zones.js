var MAX_INSTRS = 7;

function resetZones() {
	var main1 = document.getElementById('mainJ1');
	while (main1.firstChild) {
		main1.removeChild(main1.firstChild);
	}

	var node = document.getElementById('mainJ2');
	while (node.firstChild) {
		node.removeChild(node.firstChild);
	}

	node = document.getElementById('procJ1');
	while (node.firstChild) {
		node.removeChild(node.firstChild);
	}

	node = document.getElementById('procJ2');
	while (node.firstChild) {
		node.removeChild(node.firstChild);
	}

	node = document.getElementById('proc2J2');
	while (node.firstChild) {
		node.removeChild(node.firstChild);
	}

	node = document.getElementById('proc2J1');
	while (node.firstChild) {
		node.removeChild(node.firstChild);
	}
}

function loadZones(nvx) {
	resetZones();
	var htmlMain1 = '<div class="case caseMainJ1"><span class="numberRep">0</span></div>';


	var progJ1 = document.getElementById('progJ1');
	var progJ1c = progJ1.children;
	var arr = Array.from(progJ1c);

	arr.forEach(function (programs) {
		var borne;
		if (programs.id == 'mainJ1') {
			borne = config.level[nvx].program[0].main[0];
			// programs.innerHTML += '<div>Principal : </div>'
		}
		else if (programs.id == 'procJ1') {
			borne = config.level[nvx].program[0].proc1[0];
		}
		else if (programs.id == 'proc2J1') {
			borne = config.level[nvx].program[0].proc2[0];
		};


		if (borne > MAX_INSTRS) {
			borne = MAX_INSTRS;
		};
		var i;
		for (i = 0; i < borne; i++) {
			programs.innerHTML += htmlMain1;
		}

	});


	var htmlMain2 = '<div class="case caseMainJ2"><span class="numberRep">0</span></div>';


	var progJ2 = document.getElementById('progJ2');
	var progJ2c = progJ2.children;
	var arr2 = Array.from(progJ2c);

	arr2.forEach(function (programs) {
		var borne;
		if (programs.id == 'mainJ2') {
			borne = config.level[nvx].program[1].main[0];
			// programs.innerHTML += '<div class="txtInstr">Principal : </div>'
		}
		else if (programs.id == 'procJ2') {
			borne = config.level[nvx].program[1].proc1[0];
			// programs.innerHTML += '<div class="txtInstr">Proc 1 : </div>'
		}
		else if (programs.id == 'proc2J2') {
			borne = config.level[nvx].program[1].proc2[0];
			// programs.innerHTML += '<div class="txtInstr">Proc 2 : </div>'
		};


		if (borne > MAX_INSTRS) {
			borne = MAX_INSTRS;
		};
		var i;
		for (i = 0; i < borne; i++) {
			programs.innerHTML += htmlMain2;
		}

	});
}

